from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm

# Create your views here.


@login_required
def ProjectList(request):
    list = Project.objects.filter(owner=request.user)
    context = {
        "project_list": list,
    }
    return render(request, "projects/list.html", context)


@login_required
def ProjectDetails(request, id):
    details = get_object_or_404(Project, id=id)
    context = {
        "project": details,
    }
    return render(request, "projects/detail.html", context)


@login_required
def CreateProject(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
