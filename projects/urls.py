from projects.views import ProjectList, ProjectDetails, CreateProject
from django.urls import path

urlpatterns = [
    path("", ProjectList, name="list_projects"),
    path("<int:id>/", ProjectDetails, name="show_project"),
    path("create/", CreateProject, name="create_project"),
]
