from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


# Create your views here.


@login_required
def CreateTask(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()

    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


def TaskList(request):
    list = Task.objects.filter(assignee=request.user)
    context = {
        "task_list": list,
    }
    return render(request, "tasks/my_tasks.html", context)
