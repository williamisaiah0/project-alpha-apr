from django.urls import path
from tasks.views import CreateTask, TaskList

urlpatterns = [
    path("create/", CreateTask, name="create_task"),
    path("mine/", TaskList, name="show_my_tasks"),
]
